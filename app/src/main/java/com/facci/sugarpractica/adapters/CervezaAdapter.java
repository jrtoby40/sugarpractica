package com.facci.sugarpractica.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.facci.sugarpractica.Cerveza;
import com.facci.sugarpractica.R;

import java.util.List;

public class CervezaAdapter extends RecyclerView.Adapter<CervezaAdapter.CervezaViewHolder> {

    List<Cerveza> listcerveza;
    public CervezaAdapter(List<Cerveza> listcerveza){
        this.listcerveza = listcerveza;
    }

    @NonNull
    @Override
    public CervezaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cerveza_layout, parent, false);
        return new CervezaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CervezaViewHolder holder, int position) {

        holder.nombre.setText(listcerveza.get(position).getNombre());
        holder.gradoalcohol.setText(listcerveza.get(position).getGrado_alcohol());
        holder.precio.setText(listcerveza.get(position).getPrecio());



    }

    //Retorna la cantidad de registros que haya en la lista
    @Override
    public int getItemCount() {
        return listcerveza.size();
    }

    public class CervezaViewHolder extends RecyclerView.ViewHolder {

        TextView nombre, precio, gradoalcohol;
        ImageView image;

        public CervezaViewHolder(@NonNull View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.Nombre);
            precio = itemView.findViewById(R.id.precio);
            gradoalcohol = itemView.findViewById(R.id.Gradoalcohol);
            image = itemView.findViewById(R.id.imagensita);


        }
    }
}
