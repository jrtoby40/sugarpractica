package com.facci.sugarpractica;

import com.orm.SugarRecord;

public class Cerveza extends SugarRecord<Cerveza> {
    String Nombre;

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    String precio;
    String grado_alcohol;
    String foto;

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getGrado_alcohol() {
        return grado_alcohol;
    }

    public void setGrado_alcohol(String grado_alcohol) {
        this.grado_alcohol = grado_alcohol;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Cerveza(){

    }
    public Cerveza(String nombre, String precio, String grado_alcohol, String foto)

    {
        this.Nombre= nombre;
        this.precio = precio;
        this.grado_alcohol = grado_alcohol;
        this.foto = foto;
    }
}
