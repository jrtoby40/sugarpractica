package com.facci.sugarpractica;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;

import com.facci.sugarpractica.adapters.CervezaAdapter;

import java.util.List;

public class RecyclerActivity extends AppCompatActivity  {

    RecyclerView recyclerViewCerveza;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);



        recyclerViewCerveza = findViewById(R.id.RecyclerViewCerveza);

        //Coleccion de datos
        //Layaout
        //Adaptador

        List<Cerveza> cerveza = Cerveza.listAll(Cerveza.class);
        for (Cerveza cerveza1: cerveza) {
            Log.e("Datos", "Se muestra"+cerveza1.getNombre());
        }

        recyclerViewCerveza.setLayoutManager(
                new LinearLayoutManager(this)
        );
        //ADPATADOR

        CervezaAdapter cervezaAdapter = new CervezaAdapter(cerveza);
        recyclerViewCerveza.setAdapter(cervezaAdapter);


    }
}