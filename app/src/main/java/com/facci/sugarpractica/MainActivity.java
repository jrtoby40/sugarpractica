package com.facci.sugarpractica;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText editTextNombre, editTextfoto;
    EditText editTextprecio;
    EditText editTextgrados;
    Button buttonGuardar;
    Button buttonRecycler;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextNombre = findViewById(R.id.EditTextNombre);
        editTextprecio = findViewById(R.id.EditTextPrecio);
        editTextgrados = findViewById(R.id.Gradoalcohol);
        editTextfoto = findViewById(R.id.EditTextFoto);



        buttonGuardar = findViewById(R.id.ButtonGuardar);
        buttonRecycler = findViewById(R.id.ButtonMostrarRecycler);

       buttonGuardar.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Cerveza cerveza = new Cerveza(
                editTextNombre.getText().toString(),
                editTextgrados.getText().toString(),
                editTextprecio.getText().toString(),
                       editTextfoto.getText().toString()
               );
               cerveza.save();

               Log.e("SQLITE", "Datos Guardados");

           }

       });

        buttonRecycler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =  new Intent(MainActivity.this, RecyclerActivity.class);
                startActivity(intent);
            }
        });

    }
}